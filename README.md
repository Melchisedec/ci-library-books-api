# ci library books api

This is an api for books, find the endpoints in the Readme.md file, replace path with your local path

GET
http://localhost/PROJECTS/web/ci/ci-api/tutorial_2/ci/restful-services-in-codeigniter/api/bookbyIsbn?isbn=978-0201633610

http://localhost/PROJECTS/web/ci/ci-api/tutorial_2/ci/restful-services-in-codeigniter/api/books

POST
http://localhost/PROJECTS/web/ci/ci-api/tutorial_2/ci/restful-services-in-codeigniter/api/addbook

PUT
http://localhost/PROJECTS/web/ci/ci-api/tutorial_2/ci/restful-services-in-codeigniter/api/updateBook

DELETE
http://localhost/PROJECTS/web/ci/ci-api/tutorial_2/ci/restful-services-in-codeigniter/api/deleteBook


